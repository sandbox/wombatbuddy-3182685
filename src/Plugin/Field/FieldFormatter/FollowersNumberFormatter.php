<?php

namespace Drupal\social_name_with_followers_field\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'followers_number' formatter.
 *
 * @FieldFormatter(
 *   id = "followers_number",
 *   label = @Translation("Followers number"),
 *   field_types = {
 *     "social_name_with_followers"
 *   }
 * )
 */
class FollowersNumberFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      // Implement default settings.
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    return [
      // Implement settings form.
    ] + parent::settingsForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    // Implement settings summary.
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {

    $elements = [];
    $label = $this->fieldDefinition->getLabel();

    foreach ($items as $delta => $item) {

      $elements[$delta] = [
        '#theme' => 'followers_number',
        '#label' => $label,
        '#followers_number' => $item->followers_number,
      ];
    }

    return $elements;
  }

}
