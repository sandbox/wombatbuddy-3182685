<?php

namespace Drupal\social_name_with_followers_field\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'social_name_with_followers' widget.
 *
 * @FieldWidget(
 *   id = "social_name_with_followers",
 *   module = "social_name_with_followers_field",
 *   label = @Translation("Social name with followers"),
 *   field_types = {
 *     "social_name_with_followers"
 *   }
 * )
 */
class SocialNameWithFollowersWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {

    $element += [
      '#type' => 'details',
      '#collapsible' => TRUE,
      '#open' => TRUE,
      '#attributes' => ['class' => ['container-inline']],
    ];

    $element['social_url'] = [
      '#markup' => $this->getFieldSetting('social_url'),
    ];

    $element['social_name'] = [
      '#type' => 'textfield',
      '#default_value' => $items[$delta]->social_name ?? NULL,
      '#size' => 30,
    ];

    $element['followers_number'] = [
      '#type' => 'number',
      '#title' => $this->t('Followers'),
      '#default_value' => $items[$delta]->followers_number ?? 0,
      '#min' => 0,
    ];

    return $element;
  }

}
